## Test header (tentamen voorblad)

- **Teacher** Michiel Noback (NOMI), to be reached at +31 50 595 4691
- **Test size** 5 programming assignments and 2 open questions (with subquestions)
- **Aiding materials** Computer on the BIN network
- **Supplementary materials**  
    - `docs/corejava.pdf` Java cheat sheet by RefCardz
    - `data/basecaller_data.txt` Data file for one or more of the assignments
    - `question_answers.properties` a properties file to be used for simple
     textual answers to open questions.
    - `readme.pdf` this Readme as pdf.
    
- **TO BE SUBMITTED  
This project, as `zip` archive. Use this name for the zip:
 `AppdesignAssessment<YOURNAME-YOURSTUDENTNo>.zip`**

(Note to lecturer: use `pandoc Readme.md -o exam.pdf` to convert this document to pdf)

## Instructions

For this test, you should be logged in as guest (username = "gast", password = "gast"). 
On your Desktop you will find the assessment as a Gradle-managed project, 
 as well as the submit script `submit_your_work`. 
 Open this project in IntelliJ Idea and deal with the assignments. Answer any textual questions
 in the provided `question_answers.properties` file that is located within the `answers` folder. 
After finishing, create a zip according to name given above and submit it using the submit script
 `submit_your_work`.
Type `submit_your_work --help` in the terminal to get help on its usage.

The possible number of points to be scored are indicated for all assignments. Your grade 
will be calculated as  
`Grade = PointsScore < 10 ? 1 : (PointsScored / 10)`

Since this exam is graded automatically, you need to be extra careful to use correct spelling!

## The Assignments

This project has a standard Gradle-managed layout. Under `src/main/java` you will find 
one or several packages with several classes that may be referred to from within the assignments.


#### Assignment 1: The correct type (10 points)

For each of these use cases or descriptions below, give the best data type.
 Select from `short`, `char`, `int`, `long`, `boolean`, `double`, `String` and `enum`.
 to be used for that purpose. Fill this out in the corresponding line of the file
 `question_answers.properties` file. To make sure it is clear how to do it, the answer to question `a`
 is already given (`assignment1.a=String`).

a. A variable to hold the name of a user of your program
b. A counter to track the number of lines in a Fastq sequence file
c. A variable to track the dead or alive status of a laboratory animal
d. A variable to specify one the 3 different domains of life
e. A variable to represent the protein-coding area relative to the genome
f. A variable to track the number of unique amino acids in a protein sequence.


#### Assignment 2: The correct modifier (10 points)

For each of these use cases below, give the best (access) modifier.
 Select from `final`, `static`, `private`, `public`, `protected`, `default` (for default
 access which is actually no modifier) to be used for that purpose.
 Fill this out in the corresponding line of the file
 `question_answers.properties` file.

a. A variable storing the GemBank identifier of a Protein class 
b. A variable to store all possible legal (ambiguity) characters of nucleic acid sequences 
c. A method that should be accessed by subclasses in other packages, but not other classes in that package
d. A method that should not be overridden by subclasses 
e. A method that should have package visibility only


#### Assignment 3: Strings and characters (10 points)

In package `nl.bioinf` you will find class `StringUtils`. It has a method called `stringArrayToCharacterArray()`.
The method has this signature:

```java
public char[] stringArrayToUppercaseCharacterArray(String[] words)
```

This means that all words in the string array need to be converted into a single character array, all
 words in the order in which they were present in the string array.
It is your task to implement this method.


#### Assignment 4 (10 points)

In the same class, `StringUtils`, you will find another method, called `findWordWithSubstring()`.
 The method has this signature:

```java
public String findWordWithSubstring(String[] words, String substringToFind)
```

This method is supposed to find and return the word where the given substring is found, **case insensitive**.
If the given substring is not found, an IllegalArgumentException should be thrown.
It is your task to implement this method.


#### Assignment 5 (10 points)

Switching to a new topic. DNA sequencers are never really sure about the base call; it is a matter of probabilities.
In file `data/sequencer_data.txt` you will find this type of data:

```
POS	G	A	T	C
1	89	11	9	4
2	76	21	10	6
3	17	15	11	95
```

For each position in a DNA sequence, the signal intensity score for each of the four nucleotides is given. 
 You will be working on processing this data into actual basecalls - the nucleotide determination of each
 position. The controller of this process will be your `main()` method located in class Sequencer.
 
 Open in your editor the three classes `Sequencer`, `PositionIntensities` and `Basecall`.
Class `Sequencer` is the class you need to work on first. It already has some contents:

```java
public class Sequencer {

    public static void main(String[] args) {
        Sequencer sequencer = new Sequencer();
        List<PositionIntensities> intensities;
        intensities = sequencer.readIntensities("data/sequencer_data.txt");
        //OR, if you did not succeed with file reading
        //intensities = fetchBackupData();
    }

    public List<PositionIntensities> readIntensities(String intensitiesFile) {
        Path path = Paths.get(intensitiesFile);
        try(BufferedReader reader = 
                Files.newBufferedReader(path, Charset.forName("UTF-8"))){
            String currentLine = null;
            while((currentLine = reader.readLine()) != null){

                System.out.println(currentLine);
                //PROCEED FROM HERE
            }
        }catch(IOException ex){
            ex.printStackTrace();
        }
        return null;
    }

    //...more code
}
```

It is your task to implement method `readIntensities()` to read the file `data/sequencer_data.txt` and
 process it into the predefined list of PositionIntensity objects. As you can see, some basic functionality
 has already been provided.

If you fail to implement this method, you can switch to the hardcoded listing of intensities, just uncomment
 in main(), like this:

```java
intensities = sequencer.readIntensities("data/sequencer_data.txt");
//OR, if you did not succeed with file reading
intensities = fetchBackupData(); //UNCOMMENTED TO BE ABLE TO PROCEED
```

#### Assignment 6 (10 points)

Now it's time to move on to the actual basecalling. In class `PositionIntensities` you will find method
 `getBasecall()`. It has this signature:

```java
public Basecall getBasecall()
```

It is up to you to implement this method correctly according to the following specifications.
First calculate the probability of each nucleotide using this formula (example for G):

```
P(G) = score(G) / sum(score(G+A+T+C))
```

Now if the Probability of the highest-scoring nucleotide is above or equal to 0.6, you can safely call
 it as that nucleotide. Otherwise, you need to make it a Basecall with an ambiguous nucleotide ('N')
 with the probability of the highest-scoring nucleotide. Once finished, create the appropriate `Basecall` 
 instance and return it. Collect all Basecalls into a List by implementing method `executeBasecalls()` in 
 class `Sequencer`.

#### Assignment 7 (10 points)

Now it is time to determine the overall run quality of the sequencer. In order to do this, you need
 to implement method `determineRunQuality()` of the `Sequencer` class. You need to create a `RunQuality`
 object that encapsulates the average probability of the Basecalls and the number of nucleotides found in te sequence.
 If you have no valid list of Basecall objects, you can simply create one from scratch to develop this method.


